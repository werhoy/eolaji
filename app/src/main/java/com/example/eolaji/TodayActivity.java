package com.example.eolaji;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;

public class TodayActivity extends AppCompatActivity {
    TextView wordText, meanText, dateText;
    ImageButton favoriteBtn, addBtn;
    Button beforeBtn, afterBtn;
    String krdate="", readDate="", nowdate="";   int index=0;
    int[] randomNumber = new int[5];
    String[] wordString, meanString;
    ArrayList<String> wordList, meanList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_today);

        wordText = findViewById(R.id.wordText);
        meanText = findViewById(R.id.questionText);
        dateText = findViewById(R.id.dateText);
        favoriteBtn = findViewById(R.id.favoriteBtn);
        addBtn = findViewById(R.id.addBtn);
        beforeBtn = findViewById(R.id.beforeBtn);
        afterBtn = findViewById(R.id.afterBtn);

        //리스트 가져오기
        wordString = getResources().getStringArray(R.array.word);
        meanString = getResources().getStringArray(R.array.mean);

        wordList = new ArrayList<String>();
        meanList = new ArrayList<String>();

        for(String s : wordString){
            wordList.add(s);
        }
        for (String s : meanString){
            meanList.add(s);
        }

        Intent fromIntent = getIntent();
        krdate = fromIntent.getStringExtra("date");
        dateText.setText(krdate); //인텐트로 넘겨받아서 변경
        nowdate = fromIntent.getStringExtra("nowdate");


        SharedPreferences sf = getSharedPreferences("isFirst",MODE_PRIVATE);
        boolean check = sf.getBoolean("check",false);

        //Log.e(this.getClass().getName(),""+check);

        if (!check){ //앱실행이 처음이면
            randomNumber(5);
            save();
            SharedPreferences sharedPreferences = getSharedPreferences("isFirst",MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            check=true;
            editor.putBoolean("check",check);
            editor.commit();
        }

        read();
        //Log.e(this.getClass().getName(),"읽은수 "+randomNumber[0]);
        //Log.e(this.getClass().getName(),readDate+" "+nowdate);
        if (!nowdate.equals(readDate)){//날짜변경시
            randomNumber(5);
            save();
        }

        //wordText, meanText
        wordText.setText(wordList.get(randomNumber[index]));
        meanText.setText(meanList.get(randomNumber[index]));


        favoriteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //이미지 변경 후 즐겨찾기 폴더에 추가
            }
        });

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //다이얼로그 띄우고 폴더에 추가
            }
        });

        beforeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //전에 랜덤 단어 가져오기
                if (index==0){
                    Toast.makeText(getApplicationContext(),"첫번째 단어입니다",Toast.LENGTH_SHORT).show();
                    return;
                }else {
                    index--;
                    wordText.setText(wordList.get(randomNumber[index]));
                    meanText.setText(meanList.get(randomNumber[index]));
                }
            }
        });

        afterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //다음 랜덤 단어 가져오기
                if (index==4){
                    Toast.makeText(getApplicationContext(),"마지막 단어입니다", Toast.LENGTH_SHORT).show();
                    return;
                }else {
                    index++;
                    wordText.setText(wordList.get(randomNumber[index]));
                    meanText.setText(meanList.get(randomNumber[index]));
                }
            }
        });

    }//end of onCrate-------------------------------------------------------------------------------

    void back(View v){
        finish();
    }


    void save(){ //파일에 저장
        try {
            FileOutputStream fos = openFileOutput("today.txt", Context.MODE_PRIVATE);
            PrintWriter writer = new PrintWriter(fos);
            writer.println(nowdate);
            //Log.e(this.getClass().getName(),randomNumber[0]+"");
            for (int i=0; i<5 ;i++){
                writer.println(randomNumber[i]);
            }
            writer.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }//end of save----------------------------------------------------------------------------------

    void read(){ //저장한 정보 읽어오기
        String str = "", num = "" ;
        try {
            FileInputStream fis = openFileInput("today.txt");
            BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
            if ((str = reader.readLine()) != null){
                readDate = str;
            }
            num = reader.readLine();
            int i =0;
            while (num != null){
                randomNumber[i]=Integer.parseInt(num);
                i++;
                num = reader.readLine();
            }
            reader.close();
            //Log.e(this.getClass().getName(),readDate);
        }catch (IOException e){
            e.printStackTrace();
        }
    }//end of read----------------------------------------------------------------------------------

    void randomNumber(int maxExtrc){
        int extrcCount = 0;
        while(extrcCount < maxExtrc) {
            int random = (int) (Math.random() * wordList.size()); // 30개 중에 5개

            if (extrcCount == 0) {
                randomNumber[extrcCount] = random;
                extrcCount++;
            } else {
                boolean flag = true;
                for(int i = 0; i < extrcCount; i++) {
                    if (random == randomNumber[i]) {
                        flag = false;
                        break;
                    }
                }
                if (flag) {
                    randomNumber[extrcCount] = random;
                    extrcCount++;
                }
            }

        }
    }//end of randomNumber--------------------------------------------------------------------------
}
