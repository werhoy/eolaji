package com.example.eolaji;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    Button todayBtn, folderBtn, testBtn, settingBtn;
    Calendar calendar;
    String krdate=""; //순우리말 날짜
    int month=0, date = 0, dayOfWeek=0;
    String[] monthList, dateList, dayOfWeekList, wordList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        todayBtn = findViewById(R.id.todayBtn);
        folderBtn = findViewById(R.id.folderAddBtn);
        testBtn = findViewById(R.id.testBtn);
        settingBtn = findViewById(R.id.settingBtn);

        calendar = Calendar.getInstance();//오늘 날짜 정보 가져오기
        month = calendar.get(Calendar.MONTH); //0부터 11
        date = calendar.get(Calendar.DATE); //1부터 31
        dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK); //1부터 7
        wordList = getResources().getStringArray(R.array.word); //단어 리스트

        changeWord();

        todayBtn.setOnClickListener(new View.OnClickListener() { //오늘의 단어
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,TodayActivity.class).putExtra("date",krdate).putExtra("nowdate",date+""));//다음 액티비티에게 전달
            }
        });

        folderBtn.setOnClickListener(new View.OnClickListener() { //단어장
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,WordbookActivity.class).putExtra("date",krdate));
            }
        });

        testBtn.setOnClickListener(new View.OnClickListener() { //테스트
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,TestActivity.class).putExtra("date",krdate));
            }
        });

        settingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { //설정
                startActivity(new Intent(MainActivity.this,WordActivity.class).putExtra("date",krdate));
            }
        });

    }//end of onCrate-------------------------------------------------------------------------------

    void changeWord(){ //순우리말로 날짜 바꾸기
        monthList = getResources().getStringArray(R.array.monthList); //월 리스트
        dateList = getResources().getStringArray(R.array.dateList); //일 리스트
        dayOfWeekList = getResources().getStringArray(R.array.dayOfWeekList); //요일 리스트

        krdate = monthList[month]+dateList[date]+dayOfWeekList[dayOfWeek];

    }//end of changeWord----------------------------------------------------------------------------


}
