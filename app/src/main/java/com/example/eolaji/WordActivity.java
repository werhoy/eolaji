package com.example.eolaji;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

public class WordActivity extends AppCompatActivity {
    String krdate;
    int index;
    String[] wordString, meanString;
    ArrayList<String> wordList, meanList;

    TextView dateText, wordText, meanText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_word);

        dateText = findViewById(R.id.dateText);
        wordText = findViewById(R.id.wordText);
        meanText = findViewById(R.id.questionText);

        wordString = getResources().getStringArray(R.array.word);
        meanString = getResources().getStringArray(R.array.mean);

        wordList = new ArrayList<String>();
        meanList = new ArrayList<String>();

        for(String s : wordString){
            wordList.add(s);
        }
        for (String s : meanString){
            meanList.add(s);
        }

        //날짜 인덱스 가져오기
        Intent fromIntent = getIntent();
        krdate = fromIntent.getStringExtra("date");
        index = fromIntent.getIntExtra("index",0);
        dateText.setText(krdate);

        //Log.e(this.getClass().getName(),wordList.get(index));
        wordText.setText(wordList.get(index));
        meanText.setText(meanList.get(index));

    }//onCreate-------------------------------------------------------------------------------------

    void back(View v){
        finish();
    }
}
