package com.example.eolaji;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class QuestionActivity extends AppCompatActivity implements View.OnClickListener  {
    String[] wordString, meanString;
    ArrayList<String> wordList, meanList;
    int[] qrandomNumber,arandomNumber, randomIndex;
    TextView dateText, questionText, num1, num2, num3, statusText;
    String krdate="", mode="";
    int questionNumber;
    int index = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);

        dateText = findViewById(R.id.dateText);
        questionText = findViewById(R.id.questionText);
        num1 = findViewById(R.id.num1);
        num2 = findViewById(R.id.num2);
        num3 = findViewById(R.id.num3);
        statusText = findViewById(R.id.statusText);

        Intent fromIntent = getIntent();
        krdate = fromIntent.getStringExtra("date");
        questionNumber = fromIntent.getIntExtra("number",0);
        mode = fromIntent.getStringExtra("mode");
        dateText.setText(krdate);

        wordList = new  ArrayList<String>();
        meanList = new ArrayList<String>();
        wordString = getResources().getStringArray(R.array.word);
        meanString = getResources().getStringArray(R.array.mean);

        for (String s : wordString){
            wordList.add(s);
        }
        for (String s : meanString){
            meanList.add(s);
        }

        if (mode.equals("랜덤")){
            //
        }else if (mode.equals("즐겨찾기")){
            Toast.makeText(getApplicationContext(),"구현중",Toast.LENGTH_SHORT).show();
            finish();
        }

    }//end of onCreate------------------------------------------------------------------------------

    void back(View v){
        finish();
    }

    @Override
    public void onClick(View v) {

    }//end of onClick-------------------------------------------------------------------------------

}
