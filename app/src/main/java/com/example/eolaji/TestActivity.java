package com.example.eolaji;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class TestActivity extends AppCompatActivity {
    Intent fromintent;
    TextView dateText;
    EditText numberText;     int questionNumber;
    RadioGroup modeRadioBtn;    String mode="";
    Button startBtn;
    String krdate;
    String[] wordStirng;  ArrayList<String> wordList=new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        dateText = findViewById(R.id.dateText);
        numberText = findViewById(R.id.numberText);
        startBtn = findViewById(R.id.startBtn);
        modeRadioBtn = findViewById(R.id.modeRadioBtn);

        wordStirng = getResources().getStringArray(R.array.word);
        for (String s : wordStirng){
            wordList.add(s);
        }

        //날짜 변경
        fromintent = getIntent();
        krdate = fromintent.getStringExtra("date");
        dateText.setText(krdate);


        Log.e(this.getClass().getName(),"number: "+questionNumber);

        startBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //시작 버튼을 눌렀을때
                int id = modeRadioBtn.getCheckedRadioButtonId();
                RadioButton rb = findViewById(id);
                questionNumber = Integer.parseInt(""+numberText.getText().toString());
                mode = rb.getText().toString();
                if (questionNumber > wordList.size()){
                    Toast.makeText(getApplicationContext(),"입력을 확인해주세요",Toast.LENGTH_SHORT).show();
                    numberText.setText("");
                }else {
                    Toast.makeText(getApplicationContext(),"테스트를 시작합니다.",Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(TestActivity.this, QuestionActivity.class).putExtra("number",questionNumber).putExtra("date",krdate).putExtra("mode",mode));
                }

            }
        });



    }//onCreate-------------------------------------------------------------------------------------

    void back(View v){
        finish();
    }

}
