package com.example.eolaji;

import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;

public class FolderActivity extends AppCompatActivity {
    ListView folderList;
    ImageButton folderAddBtn;    TextView dateText;
    boolean change = false;
    String krdate="";
    String[] folderNameString;  ArrayList<String> folderNameList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_folder);

        folderList =findViewById(R.id.folderList);
        folderAddBtn = findViewById(R.id.folderAddBtn);
        dateText = findViewById(R.id.dateText);

        krdate = getIntent().getStringExtra("date");
        dateText.setText(krdate);


        folderList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //List 클릭 이벤트
            }
        });

        folderAddBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //폴더 생성
            }
        });

    }//end of onCreate------------------------------------------------------------------------------

    void back(View v){
        finish();
    }

}
