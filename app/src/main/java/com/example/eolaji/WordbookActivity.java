package com.example.eolaji;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class WordbookActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    String krdate;
    TextView dateText;
    ListView wordListView;
    ArrayList<String> wordList=new ArrayList<String>();
    String[] wordString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wordbook);

        dateText = findViewById(R.id.dateText);
        wordListView = findViewById(R.id.wordListView);

        wordString = getResources().getStringArray(R.array.word);
        for (String s : wordString){
            wordList.add(s);
        }

        //날짜 변경
        Intent fromIntent = getIntent();
        krdate = fromIntent.getStringExtra("date");
        dateText.setText(krdate);

        wordListView.setOnItemClickListener(this);

    }//onCreate-------------------------------------------------------------------------------------

    void back(View v){
        finish();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(WordbookActivity.this, WordActivity.class).putExtra("index",position).putExtra("date",krdate);
        Toast.makeText(getApplicationContext(),wordList.get(position),Toast.LENGTH_SHORT).show();
        startActivity(intent);
    }//onItemClick----------------------------------------------------------------------------------
}
